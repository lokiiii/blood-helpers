const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const path = require('path');
const index = require('./routes/router');
const session = require('express-session');

const app = express();

const db = require('./config/keys').MongoURI;

mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true})
    .then(()=> console.log('MongoDB Connected Globally.....'))
    .catch(err=> console.log(err))

app.use(expressLayouts)
app.set('view engine','ejs')

app.use(express.urlencoded({ extended: false}))

app.use(express.static(path.join(__dirname, 'css')));
app.use(express.static(path.join(__dirname, 'js')));
app.use(express.static(path.join(__dirname, 'img')));


const cookieObject = {
    secure: false,
    httpOnly: true,
    maxAge: 5000 * 60,
};
  
  // Set session Object
app.use(session({
    name: 'blooddd',
    secret: '36216584213541It\'s!a!secret',
    resave: false,
    saveUninitialized: false,
    cookie: cookieObject
}));

app.use('/', index);

app.use((req, res, next) =>{
    var err = new Error('File Not Found');
    err.status = 404;
    res.status(err.status || 500);
    res.send(err.message);
})

const PORT = process.env.PORT || 5000
app.listen(PORT, console.log(`Server started on port ${PORT}`))