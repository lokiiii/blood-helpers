// Redirect to login page if session is not active
module.exports.redirectLogin = (req, res, next) => {
  if (req.session.iduser) {
    return next();
  }
  let errors = []
  res.status(401);
  let iduser = '';
  return res.render('welcome',{
    errors,iduser
  });
};

// Redirect to dashboard if user is already logged in
module.exports.redirectHome = (req, res, next) => {
  if (req.session.emailID) {
    return res.redirect('/home');
  }
  return next();
};
