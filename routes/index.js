const express = require('express')
const router = express.Router()
// const auth = require('../config/auth')
const bcrypt = require('bcryptjs');
const redirector = require('../config/redirections/redirector');

router.get('/',redirector.redirectLogin, (req, res) =>{
    var iduser;
    iduser = req.session.iduser;
    // console.log('-------------***********',iduser);
    if(iduser === '' || iduser === undefined){
        // console.log(iduser,'---');
        var session = '';
        req.session.iduser = session;
        iduser = req.session.iduser;
    }
    res.render('welcome',{
        iduser
    })
})

module.exports = router