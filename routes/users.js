const express = require("express");
const router = express.Router();
// const auth = require('../config/auth')
const bcrypt = require("bcryptjs");
const mailer = require("./classes/mailer");
const redirector = require("../config/redirections/redirector");
const moment = require("moment");
var crypto = require("crypto");
const User = require("../models/User");

router.get("/login", (req, res) => {
  const email = req.session.emailID;
  const password = req.session.passwordID;
  var len;

  var mykey = crypto.createCipher("aes-128-cbc", "mypass");
  var mystr = mykey.update(password, "utf8", "hex");
  mystr += mykey.final("hex");
  // console.log(mystr,'----------',mykey);

  var errors = [];

  User.find({ email: email, password: mystr }, (err, response) => {
    if (response && response.length === 0) {
      var iduser = req.session.iduser;
      errors.push({ message: "Check your Email and Password " });
      res.redirect("/");
    } else {
      var session = response[0]._id;
      req.session.iduser = session;
      // console.log(req.session.iduser);
      // console.log(req.session);
      len = response[0].last_donated.length - 1;
      res.render("dashboard", {
        response: response[0],
        len,
        errors,
      });
    }
  });
});

router.post("/login", (req, res) => {
  const email = req.body.email_login;
  const password = req.body.password_login;

  req.session.emailID = email;
  req.session.passwordID = password;
  req.session.iduser = "";

  res.redirect("/users/login");
});

router.post("/register", async (req, res) => {
  // 568118881204 - 18 years

  newuser = req.body;
  const password = req.body.password;
  const password2 = req.body.password2;
  const pincode = req.body.pincode;
  const phone = req.body.phone;
  const dob = req.body.dob;

  var errors = [];
  // var time = new Date(dob);
  // var timestamp = time.getTime();
  // console.log(timestamp);
  // var today = Date.now();
  // // var todaystamp = today.getTime();
  // console.log(today);
  // var diff = today - timestamp;
  // console.log(diff);
  // var last_update = 568118881204;
  // var time1 = moment(last_update).format('MMM DD YYYY');
  // console.log(time1);

  if (password === password2) {
    // newuser.password = bcrypt.hashSync(password,10);
    console.log(newuser.password);

    if (pincode.length === 6 && phone.length === 10) {
      await User.find(
        { email: newuser.email, phone: newuser.phone },
        (err, result) => {
          if (result && result.length === 0) {
            var mykey = crypto.createCipher("aes-128-cbc", "mypass");
            var mystr = mykey.update(password, "utf8", "hex");
            mystr += mykey.final("hex");
            // console.log(mystr,'----------',mykey);

            newuser.password = mystr;

            console.log(newuser);

            let user = new User(newuser);

            user
              .save()
              .then((data) => {
                console.log("Database added");
              })
              .catch((err) => {
                console.log(err);
              });

            errors.push({ message: "Username Registered !!" });
            var iduser = req.session.iduser;
            res.redirect("/");
          } else {
            errors.push({ message: "Username Already Exists" });
            var iduser = req.session.iduser;
            res.redirect("");
          }
        }
      );
    } else {
      errors.push({ message: "Check your fields again" });
      var iduser = req.session.iduser;
      res.redirect("/");
    }
  } else {
    errors.push({ message: "Passwords do not match" });
    var iduser = req.session.iduser;
    res.redirect("/");
  }
});

router.post("/forgotpassword", async (req, res) => {
  function randomStr(len, bits) {
    bits = bits || 36;
    let outStr = "",
      newStr;
    while (outStr.length < len) {
      newStr = Math.random().toString(bits).slice(2);
      outStr += newStr.slice(0, Math.min(newStr.length, len - outStr.length));
    }
    return outStr.toUpperCase();
  }
  const otp = randomStr(7, 32);
  // console.log(randomStr(5,32));

  const email = req.body.email_forgot;
  req.session.emailID = email;
  req.session.iduser = otp;

  var errors = [];
  await User.find({ email: req.body.email_forgot }, async (err, result) => {
    if (result && result.length === 0) {
      errors.push({ message: "Email ID not registered " });
      var iduser = req.session.iduser;
      res.redirect("/");
    } else {
      await User.find(
        { email: req.body.email_forgot, dob: req.body.dob_forgot },
        async (err, result) => {
          if (result && result.length === 0) {
            errors.push({ message: "Date of Birth do not match" });
            var iduser = req.session.iduser;
            res.redirect("/");
          } else {
            const password = result[0].password;
            const content =
              "<p>The OTP for your registered Email ID " +
              req.body.email_forgot +
              " is <strong>" +
              otp +
              "</strong></p>" +
              "<br/><p>An attempt to change your password has been initiated<br/>Incase, you have not asked for the change please secure your account immediately</p><br/><p>Thanks & Regards,<br/>Team Blood Helper</p>";
            await mailer.mail(
              req.body.email_forgot,
              "OTP for Blood Donation",
              content
            );
            errors.push({ message: "OTP sent to your registered Email ID" });
            req.session.iduser = otp;
            let iduser = req.session.iduser;
            res.redirect("/");
          }
        }
      );
    }
  });
});

router.post("/checkotp", async (req, res) => {
  let errors = [];

  const otp = req.session.iduser;
  const email = req.session.emailID;
  const userotp = req.body.otp;
  const password = req.body.password_otp;

  var mykey = crypto.createCipher("aes-128-cbc", "mypass");
  var mystr = mykey.update(password, "utf8", "hex");
  mystr += mykey.final("hex");

  console.log(
    otp,
    "===========",
    email,
    "***************",
    userotp,
    "------------",
    password
  );

  if (otp === userotp) {
    await User.find({ email: email }, async (err, result) => {
      if (err) {
        errors.push({ message: "An error occured. Try Again !!" });
        var iduser = "";
        res.redirect("/");
      }

      await User.updateOne(
        { email: email },
        { $set: { password: mystr } },
        async (err, response) => {
          errors.push({ message: "Password updated Successfully" });
          var iduser = "";
          res.redirect("/");
        }
      );
    });
  } else {
    errors.push({ message: "You entered wrong OTP" });
    var iduser = "";
    res.redirect("/");
  }
});

router.post("/adddate", redirector.redirectLogin, async (req, res) => {
  const email = req.session.emailID;
  const password = req.session.passwordID;
  const id = req.session.iduser;
  const newdate = req.body.newdate;
  var len;
  var errors = [];
  console.log(newdate, "-------");

  console.log(
    email,
    "-------------",
    password,
    "-----------",
    id,
    "--------------",
    newdate
  );

  if (newdate !== "") {
    await User.find(
      { email: email, password: password, _id: id },
      async (err, result) => {
        // console.log(result);
        if (result.length !== 0) {
          console.log("result if");
          await User.updateOne(
            { email: email, password: password, _id: id },
            { $push: { last_donated: newdate } },
            async (err, res) => {
              // console.log(res)
            }
          );
          await User.find(
            { email: email, password: password, _id: id },
            async (err, result) => {
              len = result[0].last_donated.length - 1;
              res.render("dashboard", {
                response: result[0],
                len,
                errors,
              });
            }
          );
        }
        len = result[0].last_donated.length - 1;
        res.render("dashboard", {
          response: result[0],
          len,
          errors,
        });
      }
    );
  } else {
    await User.find(
      { email: email, password: password, _id: id },
      async (err, result) => {
        errors.push({ message: "Select a date from DatePicker" });
        len = result[0].last_donated.length - 1;
        res.render("dashboard", {
          response: result[0],
          len,
          errors,
        });
      }
    );
  }
});

router.get("/update", redirector.redirectLogin, (req, res) => {
  var errors = [];
  var iduser = req.session.iduser;

  const email = req.session.emailID;
  const password = req.session.passwordID;
  var len;

  User.find({ email: email, _id: iduser }, (err, response) => {
    if (response && response.length === 0) {
      var iduser = req.session.iduser;
      errors.push({ message: "Email ID not registered " });
      res.render("welcome", {
        errors,
        iduser,
      });
    } else {
      var session = response[0]._id;
      req.session.iduser = session;
      // console.log(req.session.iduser);
      // console.log(req.session);
      len = response[0].last_donated.length - 1;
      res.render("update_profile", {
        response: response[0],
        len,
        errors,
      });
    }
  });
});

router.post("/update", redirector.redirectLogin, async (req, res) => {
  console.log(req.body);

  let errors = [];

  const name = req.body.name;
  const email = req.body.email;
  const pincode = req.body.pincode;
  const phone = req.body.phone;
  const state = req.body.state;

  const emailID = req.session.emailID;
  const id = req.session.iduser;

  console.log(emailID, "----------------", id);

  await User.updateOne(
    { email: emailID, _id: id },
    {
      $set: {
        name: name,
        email: email,
        pincode: pincode,
        phone: phone,
        state: state,
      },
    },
    async (err, response) => {
      if (err) {
        errors.push({ message: "Check your fields again !!" });
      }
      // console.log(response);
      errors.push({ message: "Profile updated successfully !!" });
      await User.find({ email: emailID, _id: id }, (err, result) => {
        len = result[0].last_donated.length - 1;
        res.render("update_profile", {
          response: result[0],
          len,
          errors,
        });
      });
    }
  );
});

router.get("/logout", (req, res) => {
  var errors = [];
  var iduser = "";
  req.session.destroy();
  res.redirect("/");
});

router.get("/check", (req, res) => {
  const email = req.session.emailID;
  const password = req.session.passwordID;
  var len;
  var iduser = "";

  var pass = "blood-helper";

  var mykey = crypto.createCipher("aes-128-cbc", "mypass");
  var mystr = mykey.update(pass, "utf8", "hex");
  mystr += mykey.final("hex");
  console.log(mystr, "----------");

  var errors = [];

  // res.render('welcome',{
  //     iduser,errors
  // })
  //
});

module.exports = router;
