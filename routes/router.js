const express = require('express');
const router = express.Router();
const index = require('./index');
const users = require('./users');
const search = require('./search');

router.use('/',index);

router.use('/users',users);

router.use('/search',search);

module.exports = router;