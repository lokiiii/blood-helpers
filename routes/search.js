const express = require("express");
const router = express.Router();
const mail = require("../routes/classes/mailer");
const moment = require("moment");
var text = require("textbelt");
const User = require("../models/User");

router.get("/", async (req, res) => {
  const blood_group = req.app.locals.blood_group;
  const pincode1 = req.app.locals.pincode;
  let iduser = req.session.iduser;
  let errors = [];
  if (iduser === undefined) {
    req.session.iduser = "undefined";
    iduser = req.session.iduser;
  }
  // console.log(iduser);
  const pincode = pincode1 - 2;
  const pincode2 = pincode1 + 5;
  try {
    let response1 = await User.find({
      blood_group: blood_group,
      pincode: { $gt: pincode, $lt: pincode2 },
      available: "true",
    });
    const matched_entries1 = response1.length;
    console.log(response1);
    const response = [];

    // console.log(response[0].email);
    // console.log(iduser);

    for (let i = 0; i < matched_entries1; i++) {
      const len = response1[i].last_donated.length - 1;
      const date = response1[i].last_donated[len];

      const today = Date.now();
      var todaystamp1 = new Date(date);
      var todaystamp = moment(todaystamp1).format("x");
      // console.log(todaystamp);
      const difference = (today - todaystamp) / (1000 * 60 * 60 * 24);
      // console.log(difference);
      if (difference > 90) {
        response.push(response1[i]);
      }
    }
    const matched_entries = response.length;
    //   console.log(response);

    // const date = response.last_donated[-1];
    // console.log(date)
    // const arr = [];
    // arr.push(response[1]);
    // console.log(arr);

    res.render("response", {
      response,
      matched_entries,
      iduser,
      errors,
    });
  } catch (error) {
    res.redirect("/");
  }
});

router.post("/", (req, res) => {
  const blood_group = req.body.blood_group;
  const pincode = req.body.pincode;

  req.app.locals.blood_group = blood_group;
  req.app.locals.pincode = pincode;

  res.redirect("/search");
});

router.post("/mail", async (req, res) => {
  errors = [];

  const matched_entries = req.body.matched_entries;
  const email = req.body.email;
  const header = "Blood Required";
  const mail_content =
    req.body.mail_content +
    "<br/><br/><p>An attempt to contact you help has been initiated.<br/>Kindly, help the needy and response as soon as possible with your reply</p><br/><p>Thanks & Regards,<br/>Team Blood Helper</p>";
  await mail.mail(email, header, mail_content);

  res.redirect("/search");
});

router.get("/logout", (req, res) => {
  req.session.destroy();

  const blood_group = req.app.locals.blood_group;
  const pincode = req.app.locals.pincode;
  // const iduser = req.session.iduser;

  res.redirect("/search");
});

module.exports = router;
