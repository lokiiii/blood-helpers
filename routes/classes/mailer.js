const nodemailer = require("nodemailer");

async function mail(to_mail, mail_subject, mail_content) {

    const mailOptions = {
        to: to_mail,
        from: 'bloodhelper03@gmail.com',
        subject: mail_subject,
        html: mail_content
    };

    const smtpTransport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'bloodhelper03@gmail.com',
          pass: 'helperblood03'
        }
    });

    return new Promise((resolve, reject) => {
        smtpTransport.sendMail(mailOptions, function (err, result) {

            if (err) {
                reject(0);
            }
            else {
                resolve(1)
            }
        })
    })
}

module.exports = {
    mail,
}
