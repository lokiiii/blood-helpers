# Blood Helper

It is a simple project to search for available blood donors in your Pin Code.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need Node.js framework in your running machine
And a stable internet connection

### Installing

You can run this application by installing Node.js in your local machine (If you don't have Node.js).

Run the following commands in CLI from the folder location, to open the project on localhost.
1. npm i   or   npm install
2. npm start
3. Go to your preferred web browser and type http://localhost:5000 to start the web application.