const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    blood_group:{
        type:String,
        required: true
    },
    state:{
        type:String,
        required:true
    },
    pincode:{
        type:Number,
        required: true
    },
    last_donated:{
        type:Array
    },
    dob:{
        type:Date,
        required: true
    },
    phone:{
        type:Number,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    available:{
        type:Boolean,
        required:true
    }
})

const User = mongoose.model('Blood_User', UserSchema);

module.exports = User